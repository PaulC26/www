import * as Popper from "@popperjs/core"
import * as Bootstrap from "bootstrap"

// Allow other scripts from using Bootstrap
window.Bootstrap = Bootstrap
