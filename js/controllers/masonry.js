import Masonry from "https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/+esm"

const init = () => {
  window.masonry = new Masonry("#entries", {
    percentPosition: true,
    itemSelector: '.masonry-item:not(.d-none)'
  })
}

if (document.readyState !== "loading") {
  init()
} else {
  document.addEventListener("DOMContentLoaded", init)
}
