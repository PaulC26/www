const container = document.querySelector('#entries')
const entries = container.querySelectorAll('article[data-id]')
const tagContainer = document.querySelector('#tags')
const tags = document.querySelectorAll('.js-tag-toggler')
const reset = tagContainer.querySelector('#tag-reset').innerText.trim()

const filterByTags = function(e) {
  if (!e.target.closest(".js-tag-toggler")) {
    return false
  }
  const tag = e.target.innerText.trim()
  if (tag == reset) {
    tags.forEach((tag) => tag.classList.remove('active'))
    entries.forEach((item) => item.classList.remove('d-none'))
  } else {
    tags.forEach((tag) => tag.classList.remove('active'))
    e.target.classList.add('active')

    entries.forEach(entry => {
      const entryTags = entry.dataset.tags.split(',')
      const status = entryTags.indexOf(tag) === -1
      entry.classList.toggle('d-none', status)
    })
  }
  if (window.masonry) {
    window.masonry.reloadItems()
    window.masonry.layout()
  }
  var accordion
  if (accordion = window.Bootstrap.Collapse.getInstance(tagContainer)) {
    accordion.hide()
  }
}
document.addEventListener('click', filterByTags)
