(function () {
  const link = document.querySelector("#location")
  const location = link?.getAttribute("href")
  if (location) {
    window.location = location
  }
})()
