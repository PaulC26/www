import { select } from "https://cdn.skypack.dev/d3-selection@3"
import { arc } from "https://cdn.skypack.dev/d3-shape@3"
import { scaleBand } from "https://cdn.skypack.dev/d3-scale@4"

const bars = 100 // Controls how many bars are created
const data = [...Array(bars).keys()]
const colors = [
  "#1f77b4",
  "#ff7f0e",
  "#2ca02c",
  "#d62728",
  "#9467bd",
  "#8c564b",
  "#e377c2",
  "#7f7f7f",
  "#bcbd22",
  "#17becf"
]
const svg = select('.graphique-entrepreneures svg')
const innerRadius = 5
const outerRadius = svg.attr('width') * 0.95

const x = scaleBand()
  .range([0, 2 * Math.PI])
  .align(0)
  .domain(data)

svg
  .append("g")
    .attr('transform', `translate(${outerRadius / 2},${outerRadius / 2})`)
  .selectAll("path")
  .data(data)
  .join("path")
    .attr("fill", (i) => colors[i % 10])
    .attr("d", arc()     // imagine you're doing a part of a donut plot
      .innerRadius(innerRadius)
      .outerRadius((d) => Math.max(Math.round(Math.random() * 100), Math.round(Math.random() * (outerRadius / 2))))
      .startAngle((i) => x(i))
      .endAngle((i) => x(i) + x.bandwidth())
      .padAngle(0.04))
