const people = require("./people")

module.exports = async function() {
  return await people().then((people) => people.filter((person) => !person.support))
}
