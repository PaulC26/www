const people = require("./people.js")

module.exports = async function() {
  const all = await people()
  return all.filter((person) => person.support)
}
