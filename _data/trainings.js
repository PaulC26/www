const EleventyFetch = require("@11ty/eleventy-fetch")
const { FORMASOL_API_ACCESS_TOKEN } = process.env

module.exports = async function() {
  return await EleventyFetch("https://formasol.solstice.coop/api/catalogue.json", {
    duration: '3h',
    type: 'json',
    fetchOptions: {
      headers: {
        "Authorization": `Token token=\"${FORMASOL_API_ACCESS_TOKEN}\"`
      }
    }
  })
};
