## **Ordre du jour**

L'intention est de repartir avec :

- des pistes sur quoi garder, quoi changer, quoi jeter
- des réponses aux questions de Valérie
- des exemples d'usages du site web
- répondre à la question : à quoi sert le site web solstice.coop ?
- des accès/un contact technique pour exporter le site sur un ordinateur de développement (le mien et celui de Sandra)

## Inspirations

### inspirations graphiques
- http://loffice.coop/accueil/
- https://www.les-communs-dabord.org/
- https://dime-shs.sciencespo.fr/#home
- http://www.observatoire-culture.net/#home
- https://aoc.media/
- https://www.smartfr.fr/smart/
- http://manufacture.coop/presentation/
- http://manufacture.coop/ressources/
- https://omnicite.fr/

### inspirations architecture

- https://www.coopaname.coop/
- https://www.owen.coop/
- https://dime-shs.sciencespo.fr/#home




## **Les pistes sur quoi garder, quoi changer, quoi jeter**



* Il est important de mettre en avant le modèle des SCOP sur le site
* Mettre en avant l’aspect généraliste de SOLSTICE
* Respecter la structure du site (voir le fichier Excel)
* Il faudrait enlever les dates sur les articles mis en avant dans la catégorie Actualités


## **Les exemples d’usages du site web**

* Publication d’articles sur la vie de la coopérative
* Accès aux [ressources solsticiennes ](https://solstice.coop/ressources-solsticiennes/)
* Accès aux sections dédiées aux indépendants et micro-entrepreneurs, ou encore aux personnes souhaitant devenir entrepreneures-salariées voire créatrices d’entreprise


## **A quoi sert le site web solstice.coop ?**

- Site vitrine, qui donne envie d'en savoir plus, qui donne envie de s'inscrire à une réunin d'info collective, qui déclenche la réflexion sur ce que le modèle CAE propose.
- Envie/Nécessité d'une dimension plus politique dans les contenus. — Cf. Carnet Aambassadrice·deur

A informer sur la vie de la coopérative, mettre en avant le réseau d’entrepreneurs grâce à l’annuaire, et à permettre aux futurs entrepreneurs de connaître les étapes à suivre pour rejoindre la coopérative.

_Tout est éparpillé sur le site de Solstice._

## Des pistes sur : quoi garder, quoi changer, quoi jeter

- Garder ce qui est gardable tel quel, ou ce qui doit être réécrit
- Cesser de vouloir être un site "encyclopédique". C'était la visée initale : "écrémer", filtrer. En gros lisez, et voyez si vraiment c'est votre projet.
- Volet "Combien ça coûte" : Bien détailler ce que la contribution coop signifie et recouvre :
    - Contribution au fonctionnement : concerne tous les ES
    - Contribution à la mutualisation des risques (CESA) : 
    - Contribution aux services : formations internes, rencontres coop, veille AP, OF certifié Qualiopi… Contribution financière ou échanges d'expertises.
    - Contribution au capital (associé·es)
    - ? Expliciter la notion de fin d'exercice comptable
- Tout passer en inclusif
- Jeter le compliqué, ou ce que l'on a tenté d'expliqué et qui se comprend très bien autrement et ailleurs que sur le site.

## Des exemples d'usages du site web

- Formulaires de demandes de renseignement faciles à activer, utiliser.
- Un espace "membre" qui ouvre ensuite sur : Louty, LinkedIn, Facebook… dans l'idée d'avoir un seul point d'entrée qui ouvre sur une page qui donne accès à TOUT L'UNIVERS ! Y compris toutes les newsletters.

## **Questions/Commentaires divers(es)**

* Est-ce qu’on repart à zéro ou est-ce qu’on utilise cela ?
* Quel rétro-planning ?
* Que pourriez-vous me proposer et dans la façon de faire ?

_J’ai apprécié les liens que t’avais envoyés_

[https://dime-shs.sciencespo.fr/#home](https://dime-shs.sciencespo.fr/#home)


## **Actions à suivre**

* Prototype sur une page blanche à créer
* Obtenir l’accès de SCOPEN (code FTP, base de données, compte utilisateur sur WordPress)
* Envoi d’un mail récapitulatif (Thomas)
