---
title: Comment rejoindre la coopérative d'activité Solstice ?
eleventyNavigation:
  key: Nous rejoindre
  parent: Accueil
  order: 5
---

# Nous rejoindre

C’est ici que commence votre projet en trois temps :

1. **Assister à une réunion d’information collective** \
Les réunions collectives ont lieu chaque mois, alternativement à l’Écosite de Eurre, Valence et en Visio web.
Elles ont pour objet de présenter le modèle social, économique et coopératif de la CAE Solstice, le mécanisme de contribution et le parcours entrepreneurial lié au statut d'entrepreneur·e-salarié·e.

<div class="card-group">
{% for event in calendars.infoColl -%}
  <div class="card mb-2">
    <div class="card-body">
      <h3 class="h5 upcase-first">{{ event.summary }}</h3>
      <i class="fas fa-calendar" aria-hidden="true"></i>
      <b>{{ event.startDate | toDateTime }}</b> ({{ event.duration | durationToTime }})
      <br />
      {{ event.location | location('En visio', 'Plan et directions') | safe }}{{- "" -}}
    </div>
    <footer class="card-footer">
      <a href="{{ '/nous-rejoindre/reunion-information/' }}?event={{ event.uid }}" class="card-footer-item">Je m'inscris</a>
    </footer>{{- "" -}}
  </div>{{- "" -}}
{%- endfor -%}
</div>

  [Participer à la prochaine réunion](/nous-rejoindre/reunion-information/){.btn .cta}

2. **Affiner votre projet avec votre futur·e accompagnante**
[Prendre rendez-vous avec un·e accompagnant·e suite à une réunion d'info](/contact/#accompagnant){.btn .cta}

3. **Démarrer votre contrat**

## Besoin d'échanger de vive voix avant toute chose ?

Nous répondons au téléphone les lundis, mardis, jeudis et vendredis, de 9h à midi : <a href="tel:+33475253230">04 75 25 32 30</a>
