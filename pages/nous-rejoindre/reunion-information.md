---
title: Je m'inscris à une réunion d'information collective chez Solstice
eleventyNavigation:
  key: Formulaire d'inscription
  parent: Nous rejoindre
---

{% from "form.html" import form with context %}

{{ form(id=4, calendar=calendars.infoColl, submit="Je m'inscris") }}
