---
title: Vie coopérative
eleventyNavigation:
  key: Vie coopérative
  parent: Accueil
  order: 2
---

# Vie coopérative

S’informer, se rencontrer, découvrir la coopérative et faire communauté.

[Les prochains événements](./agenda/){ .btn .cta }

[Les temps forts](./temps-forts/){ .btn .cta }

## Vous êtes Solsticien·nes, vous avez signé un CAPE ou un CESA ?
Vous avez des interlocuteur·trices dédié·es qui constituent l'équipe d'appui de la coopérative. Vous pouvez prendre rendez-vous avec vos accompagnant·es pour un entretien individuel : la loi ESS de juillet 2014, qui cadre les contrats de travail signés en CAE, prévoit un minimum de deux entretiens annuels. Vous pouvez être évidemment reçu·es en entretien individuel à votre demande (prise de rdv ci-dessous).

## Prendre rendez-vous avec votre accompagnant·e ? { #accompagnant }

{% include 'contact/rendez-vous-accompagnant.njk' %}


## Cinq bonnes pratiques entrepreneuriales + une !
1. **Faire ses notes de frais régulièrement et vérifier le bon paiement de vos client·es**

   Soyez sympa avec vos comptables préféré·es, évitez-leur les raz-de-marée de tickets de fin d’année !
   Pour clarifier et fluidifier nos échanges et obtenir rapidement TOUTES les réponses à TOUTES questions relatives à :
   - vos devis et factures de vente de vos prestations
   - vos notes de frais, vos investissements (immobilisations)
   - vos salaires, vos congés payés, vos arrêts maladie
   - vos conventions, vos demandes de financements, Bilan pédagogique, Qualiopi.

   Ce sont bel et bien des personnes humaines qui vous répondront toujours, Élise, Philippe et Marion en l'occurence.

2. **S’inscrire et venir aux formations, aux ateliers, aux rencontres**

   Ces temps de travail, pensés en équipe, font partie des moyens que nous mutualisons.

   [Visualiser les ateliers et formations ici](https://solstice.coop/cooperative/agenda/){ .btn .cta }

3. **Ne pas hésiter à solliciter un rendez-vous d’accompagnement**

   Laurie, Sarah et Bertrand sont à votre écoute en cas de doute, de baisse de motivation ou de besoin de clarification. Laurie est également référente Formation.

   [Prendre rendez-vous ici](https://solstice.coop/contact/#accompagnant){ .btn .cta }

4. **Consulter régulièrement sa trésorerie et les règlements de ses clients**

   - La tréso est un indicateur facilement accessible via le tableau de bord Activité sur Louty. Connaître sa tréso, c’est éviter les mauvaises surprises et garder en tête ses objectifs en terme d’investissement.
   - Vérifier régulièrement les paiements de ses clients : vendre c’est bien, se faire payer, c’est mieux !

5. **S’informer des actualités de la coopérative et faire communauté**

   Informez-vous via les newsletters, les rencontres, l’agenda. Ainsi vous savez ce qui se fait, se trame et se joue dans votre entreprise partagée. S’informer c’est aussi profiter / bénéficier de ce qu’entreprendre en #CAE permet.

   [Les événements collectifs de notre vie coopérative sont restitués ici](./temps-forts/){ .btn .cta }

6. **Les moyens que nous mutualisons sont au service de tout·es**

   Chez Solstice, nous cherchons à alléger le plus possible nos processus administratifs et à fluidifier le travail de l’équipe, ceci afin de pouvoir consacrer plus de temps à faire plus de choses intéressantes pour toute la communauté.

   Chaque membre de la coop agit en co-responsabilité avec une communauté, que ce soit visible, comme lorsque nous travaillons ensemble, ou invisible, comme lorsque on émet une facture ou qu’on relance un client.
