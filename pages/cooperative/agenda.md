---
title: Événements, ateliers et entraide coopérative
eleventyNavigation:
  key: Les prochains événements
  parent: Vie coopérative
---

# Vie coopérative

S’informer, se rencontrer, découvrir la coopérative et faire communauté.

[Les prochains événements](/cooperative/agenda/){ .btn .cta .active }

[Les temps forts// restitutions](/cooperative/temps-forts/){ .btn .cta }

## Réunions d'information collective

<div class="card-group">
{% for event in calendars.infoColl -%}
  <div class="card mb-2">
    <div class="card-body">
      <h3 class="h5 upcase-first">{{ event.summary }}</h3>
      <i class="fas fa-calendar" aria-hidden="true"></i>
      <b>{{ event.startDate | toDateTime }}</b> ({{ event.duration | durationToTime }})
      <br />
      {{ event.location | location('En visio', 'Plan et directions') | safe }}{{- "" -}}
    </div>
    <footer class="card-footer">
      <a href="{{ '/nous-rejoindre/reunion-information/' }}?event={{ event.uid }}" class="card-footer-item">Je m'inscris</a>
    </footer>{{- "" -}}
  </div>{{- "" -}}
{%- endfor -%}
</div>

## Ateliers, formations et rencontres coopératives

<div class="row agenda">
{%- for event in calendars.vieCooperative -%}
<div class="col-md-4">{{- "" -}}
  <div class="card mb-2">{{- "" -}}
    <div class="card-body">
      <h3 class="h5 upcase-first">{{ event.summary }}</h3>
      {%- if event.description -%}
        <p>{{ event.description }}</p>
      {%- endif -%}
      <i class="fas fa-calendar" aria-hidden="true"></i>
      {% if event.duration.toSeconds() === 86400 -%}
        <b>{{ event.startDate | toDate }} {{ event.duration | durationToTime }}</b>
      {%- else -%}
        <b>{{ event.startDate | toDateTime }}</b>
        {%- if event.duration.hours > 0 %}
          ({{ event.duration | durationToTime }})
        {%- endif -%}
      {%- endif -%}
      {%- if event.location -%}
        <br>{{ event.location | location('En visio', 'Plan et directions') | safe }}
      {%- endif -%}
    </div>{{- "" -}}
  </div>{{- "" -}}
</div>
{%- endfor -%}
</div>

