---
title: Des temps forts qui ont marqué la vie coopérative
permalink: cooperative/temps-forts/
eleventyNavigation:
  key: Les temps forts
  parent: Vie coopérative
---

S’informer, se rencontrer, découvrir la coopérative.

[Les prochains événements](/cooperative/agenda/){ .btn .cta }

[Les temps forts](/cooperative/temps-forts/){ .btn .cta .active }

{% for post in collections.posts %}
<div class="card shadow-sm shadow-hover mb-4 position-relative temps-fort">
  <div class="card-body">
    <p class="card-title">
      <a href="{{ post.url }}" rel="permalink" class="stretched-link">
        <time datetime="{{ post.date.toISOString() }}">{{ post.date | toDate }}</time>
        <span>{{ post.data.title }}</span>
      </a>
    </p>
  </div>
</div>
{% endfor %}
