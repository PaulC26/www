---
title: "Séminaire du 4 avril 2017 : Quels réseaux pour qui ?"
subtitle: 4 avril 2017
carousel:
  - img/temps-forts/2017-04-04/p20170404-095248.jpg
  - img/temps-forts/2017-04-04/p20170404-094031.jpg
  - img/temps-forts/2017-04-04/p20170404-094101.jpg
  - img/temps-forts/2017-04-04/IMG_0235.jpg
  - img/temps-forts/2017-04-04/p20170404-094143.jpg
  - img/temps-forts/2017-04-04/p20170404-094224.jpg
  - img/temps-forts/2017-04-04/p20170404-095220.jpg
  - img/temps-forts/2017-04-04/p20170404-100629.jpg
  - img/temps-forts/2017-04-04/p20170404-153135.jpg
  - img/temps-forts/2017-04-04/p20170404-153215.jpg
  - img/temps-forts/2017-04-04/p20170404-153619.jpg
  - img/temps-forts/2017-04-04/p20170404-153657.jpg
  - img/temps-forts/2017-04-04/IMG_0236-e1492092033340.jpg
  - img/temps-forts/2017-04-04/IMG_0238-e1492092055745.jpg
  - img/temps-forts/2017-04-04/IMG_0241-e1492092082576.jpg
  - img/temps-forts/2017-04-04/IMG_0242-e1492092101105.jpg
  - img/temps-forts/2017-04-04/IMG_0243-e1492092117430.jpg
  - img/temps-forts/2017-04-04/IMG_0246-e1492092139396.jpg

---

![](img/temps-forts/2017-04-04/Carton_save_the_date_avril_2017-1-768x768.jpg)

**Retours sur notre séminaire de printemps**

En première partie de journée, nous avons bénéficié d’un temps de présentation de Solstice CAE,
agrémenté des témoignages des entrepreneurs solsticiens précisant les raisons de leur entrée dans la coopérative et leurs motivations.

Ces interventions furent suivies d’une présentation bien rythmée, en ateliers, des réseaux d’entrepreneurs (Dynabuy, Carbao, Écobiz, Entr’Actifs, CPME et BNI)
et des espaces de coworking répartis sur le territoire drômois (voir récap. plus loin).

Mise en illustration de Lison Bernet.

**Espaces de coworking dans la Drôme**

<a href="http://www.cedille.pro/" target="_blank">Cedille</a> est LE réseau des espaces de coworking dans la Drôme.
Tous ces lieux y sont référencés et décrits :
- L’Usine Vivante,
- La Forge Collective,
- le 36,
- le 8+FabLab,
- Le Moulin Digital,
- Le Hublot,
- L’Atelier,
- l’Epi-Centre,
- Espace-coorking,
- Travail associé.

## Réseaux d’entrepreneurs

- <a href="http://bni26.fr/" target="_blank">BNI</a>
  Réseau de recommandations d’affaires en Drôme.
  Un Groupe BNI, ce sont des entrepreneurs souhaitant développer leurs ventes, un seul Membre par profession, des résultats mesurés, un état d’esprit partagé, une solidarité entre ses Membres et une méthode éprouvée.

- <a href="https://www.carbao.fr/" target="_blank">CARBAO</a>
  Club d’affaires de recommandation par le bouche à oreille, accélérateur d’affaires et relations humaines dans les échanges au sein du réseau.

- <a href="http://www.cgpme-ra.org/drome" target="_blank">CPME</a>
  La CPME Drôme est une organisation patronale interprofessionnelle qui représente et défend les intérêts des TPE-PME de l’Industrie, du Commerce, des Services et de l’Artisanat.

- <a href="http://www.dynabuy.fr/" target="_blank">DYNABUY</a>
  Communautés d’entreprises et de CE.

- <a href="http://drome-ecobiz.biz/jcms/j_6/fr/accueil" target="_blank">ÉCOBIZ</a>
  Drôme Ecobiz est un réseau de décideurs locaux qui s’organise autour de « communautés » thématiques ou sectorielles.
  Chaque membre s’inscrit en fonction de ses centres d’intérêts.

- <a href="https://www.facebook.com/entractif/" target="_blank">ENTR’ACTIFS</a>
  Un réseau de plus de 30 entrepreneurs pour développer son activité en Drôme, Ardèche et Isère.

L’après-midi fut consacré à quelques informations d’ordre général sur votre coopérative et quatre ateliers :
- La qualité en formation : nouveautés et nouvelles contraintes.
- Réussir ses appels d’offres en CAE : nouvelle procédure et Formations/déj.
- Le parcours d’Entrepreneur Salarié Associé, à destination des entrepreneurs bénéficiaires d’un CAPE.
- Développer son activité dans Solstice CAE : quels repères de gestion pour une dynamique entrepreneuriale ?

Ce séminaire de printemps fut enfin et surtout l’occasion de discuter entre nous et d’inclure des invités à nos rencontres et nos échanges.
Ce fut une formule ouverte à tous en matinée, que nous avons bien l’intention de réitérer en automne.
