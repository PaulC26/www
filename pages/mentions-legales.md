---
title: Mentions légales
---

<strong>SOLSTICE <abbr title="Société Anonyme">SA</abbr> <abbr title="Société COopérative de Production">SCOP</abbr></strong>

<ul class="list-unstyled">
  <li>
    <i class="fas fa-id-badge" aria-hidden="true"></i>
    Direction de publication : Bertrand Barrot
  </li>
  <li>
    <i class="fas fa-server" aria-hidden="true"></i>
    Hébergement : <a href="https://www.scopen.fr/">Scopen</a>
  </li>
  <li>
    <i class="fas fa-phone" aria-hidden="true"></i>
    Accueil téléphonique : <a href="tel:+33475253230">04 75 25 32 30</a>
  </li>
  <li>
    <i class="fas fa-envelope" aria-hidden="true"></i>
    Email : <a href="mailto:accueil@solstice.coop">accueil@solstice.coop</a>
  </li>
  <li>
    <i class="fas fa-map-marker" aria-hidden="true"></i>
    Adresse : <span class="user-select-all">Écosite, 25 Ronde des alisiers, 26400 Eurre</span>
  </li>
  <li>
    <i class="fas fa-store" aria-hidden="true"></i>
    Code <abbr title="Taxe sur la Valeur Ajoutée">TVA</abbr> : <span class="user-select-all">FR 54 438 279 382</span>
  </li>
  <li>
    <i class="fas fa-landmark" aria-hidden="true"></i>
    <abbr>SIRET</abbr> : <span class="user-select-all">438 279 382 00040</span>, immatriculé au <abbr title="Registre du Commerce et des Sociétés">RCS</abbr> de Romans
  </li>
  <li>
    <i class="fas fa-stamp" aria-hidden="true"></i>
    Code <abbr title="Activité Principale Exercée">APE</abbr> : <span class="user-select-all">7022Z</span> (Conseil pour les affaires et autres conseils de gestion)
  </li>
  <li>
    L'organisme de formation Solstice est déclaré sous le numéro d'activité <span class="user-select-all">82260114826</span>.
  </li>
</ul>
