---
title: Annuaire d'entrepreneur·es de la vallée de la Drôme
eleventyNavigation:
  key: Les entrepreneur·es
  parent: Accueil
  order: 4
scripts:
  - /js/controllers/masonry.js
  - /js/controllers/tags.js
---

:::info Annuaire en cours de réalisation
L'annuaire détaillé est en train d'être alimenté par la centaine d'entrepreneur·es de la coopérative.
:::

{% from "partials/_tags_filter.njk" import tags_filter %}
{{ tags_filter(entrepreneures | extractTags) }}

{% from "partials/_tags_list.njk" import tags_list %}
{%- set max_tags = 6 -%}

<div id="entries" class="row entrepreneures">
{% for person in entrepreneures %}
  <article class="col-sm-6 col-lg-4 mb-4 masonry-item" data-id="{{ person.id }}" data-order="{{ loop.index0 }}" data-tags="{{ person.tag_names }}">
    <div class="card shadow-hover">
      {%- set name = [person.first_name, person.last_name] | join(' ') | capitalize -%}
      {%- set link = ["/entrepreneures/", (name | slugify), "/"] | join("") -%}
      {%- set src = person.photo_link or person.logo_link -%}
      {%- if src -%}
        <a href="{{ link }}" tabindex="-1">
          {% image src, name, { class: "card-img-top h-auto" } %}
        </a>
      {%- endif -%}
      <div class="card-body">
        <h2 class="card-title text-center text-capitalize">
          <a href="{{ link }}">
            {{ name }}
          </a>
        </h2>
        <div class="card-text">{{ person.title }}</div>
        {{ tags_list(person.tag_names, max_tags) }}
      </div>
    </div>
  </article>
{% endfor %}
</div>
