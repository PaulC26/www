---
title: Protection des données
---


**CHARTE DE CONFIDENTIALITÉ – RGPD**

Dans le cadre de ses activités de Coopérative d’activités et d’Emploi (CAE), SOLSTICE SCOP est amené à traiter des informations et des données personnelles vous concernant, dont certaines sont de nature à vous identifier, par le biais de formulaires de contact sur le site : [https://solstice.coop/](https://solstice.coop/) , lors d’évènements (séminaires, formations…), documents d’ordre administratifs ou contractuels (convention de services ou de formation…), échanges par courriels, courrier ou téléphone.

Toutes les informations personnelles collectées nous permettent de traiter vos demandes administratives et/ou contractuelles, d’assurer nos prestations de formation et d’améliorer la qualité de nos services.

Dans le cadre du [Règlement général sur la protection des données (RGPD)](https://www.cnil.fr/fr/reglement-europeen-protection-donnees) et de [la loi informatique et libertés du 6 janvier 1978 modifiée](https://www.cnil.fr/fr/loi-78-17-du-6-janvier-1978-modifiee), **SOLSTICE SCOP** s’engage à ce que le recueil de vos données personnelles et leurs traitements soient conformes à ce règlement en matière de protection et de sécurité.

Pour garantir le niveau optimal de protection et sécurisation de vos informations, cette charte vous renseigne sur la manière dont nous traitons ces données personnelles. Il est important de lire attentivement ce document. Cette charte peut être modifiée et évoluer à tout moment, il est donc recommandé de la consulter régulièrement. Si des modifications importantes et significatives sont apportées à cette charte vous en serez informés par courriel ou une information-avertissement sur notre site internet : [https://solstice.coop/](https://solstice.coop/).

**<u>1\. Finalités d’utilisation de vos données :</u>**

Les données personnelles collectées ne sont utilisées que dans le cadre de la réglementation RGPD en vigueur :

*   L’exécution d’un contrat, d’une prestation ou d’une convention que nous avons conclues avec vous ;
*   Le respect d’une obligation légale et/ou réglementaire ;
*   Votre consentement à l’utilisation de vos données dans un cadre précis ;
*   Fournir, transmettre, communiquer des informations sur les services, évènements, catalogues ou prestations fournies par SOLSTICE SCOP.

Vos données personnelles ne sont utilisées que dans le cadre de votre activité, des contrats signés ou de votre projet professionnel.

**<u>2\. Données Personnelles collectées :</u>**

**2.1\. Données transmises directement :**

Elles sont collectées lors de demandes d’informations par le biais de : formulaires de contacts, collecte de coordonnées lors de séminaires ou évènements, sessions de formations, demandes d’informations sur nos services ou prestations.

Ces données personnelles collectées sont notamment :

*   Nom, Prénom, Civilité, Date de naissance
*   Adresse postale, Courriel, numéros de téléphones, fixe et/ou mobile
*   Statut > Dirigeant, salarié, demandeur d’emploi, particulier
*   Statut social à l’entrée > Demandeur d’emploi, RSA, salariés, autres…

**2\. 2\. Données collectées => relations contractuelles et/ou commerciales :**

Ce sont ici toutes les données personnelles nécessaires à la bonne gestion de la relation commerciale et/ou contractuelle, ainsi qu’au suivi des formations :

*   Toutes les données de facturation, contrats, commandes, devis, conventions…
*   Informations sur le suivi administratif, règlementaire et le financement des formations.

**2.3\. Données sensibles**

**SOLSTICE SCOP** ne collecte aucune donnée sensible et, si par erreur de telles informations étaient communiquées, elles seraient immédiatement supprimées.

Sont considérées comme sensibles les données suivantes :

*   origines raciale ou ethnique
*   opinions politiques
*   croyances religieuses ou philosophiques
*   adhésion à un syndicat
*   données relatives à la santé ou l’orientation sexuelle

**2.4\. Données collectées automatiquement en ligne :**

**SOLSTICE SCOP** ne collecte aucune donnée et information automatiquement ni aucun fichier de type cookie (fichier stocké sur votre ordinateur lors de la consultation de pages WEB, ils sont gérés par votre navigateur et seul l’émetteur peut les lire ou les modifier).

Les informations collectées via nos formulaires de contact ne sont pas stockées sur internet et nous sont communiquées par courriel.

Des boutons de liens et de partages pointant vers divers réseaux sociaux (Facebook, tweeter, google+…) sont présents sur notre site Web qui disposent chacun de leur politique de gestion des cookies et de leur propre politique de sécurité et protection des données personnelles.

**3\. Traitements et opérations sur les données collectées**

En fonction des activités, différents traitements sont susceptibles d’être opérés sur vos données personnelles : création, mise à jour, consultation, extraction, stockage, sauvegardes, suppression au terme de la durée de conservation et transferts vers des tiers, sous-traitants, prestataires restreints aux critères des destinataires mentionnés au paragraphe 4.

**4\. Responsables et destinataires des données collectées**

Ceux-ci peuvent être internes ou externes à **SOLSTICE SCOP :**

*   Nos équipes internes (responsables de traitements, collaborateurs) sont soumis à des règles de confidentialité et différents niveaux d’habilitation pour déterminer à quelles données ils ont accès :
    *   Les collaborateurs en charge de l’administratif et de la facturation
    *   Les collaborateurs en charge du pôle formation
    *   Les collaborateurs en charge des ressources humaines
    *   Les administrateurs élus par les associés de Solstice
*   Nos fournisseurs et partenaires assurant les prestations pour notre compte tous situés en France et dans l’union européenne :
    *   La paye, la comptabilité, l’informatique
    *   L’animation de formations
    *   L’URSCOP, la région AuRA et l’Union Européenne
*   Les différentes autorités de police et douane, les autorités judiciaires, les autorités administratives dans le cadre d’obligations légales de garantie de droits, de biens ou de sécurité pour **SOLSTICE SCOP.**

**SOLSTICE SCOP** s’engage à ne pas communiquer, diffuser ni transférer vos données à des tiers quel que soit le moyen utilisé, sauf :

*   dans le cadre d’obligation légale ou réglementaire comme stipulé dans l’article 6.1-c du RGPD ;
*   à des fins d’exécution de la convention de formation ou mesures précontractuelles prises à votre demande => Article 6.1-b du RGPD ;
*   ou si vous avez donné votre accord explicite (consentement) au titre de l’Article 6.1-a du RGPD.

**5\. Durée de conservation des données collectées**

Vos données sont conservées pendant une durée conforme aux dispositions légales ou une durée adaptée à la finalité des traitements pour lesquels elles ont été enregistrées.

*   Pour une relation contractuelle de prestation de services, de convention de formation : dix ans après la fin du dernier contrat ou contact, sauf si une disposition réglementaire supérieure le requiert (duplicata de facture, vérifications administratives, juridiques ou fiscales, DIRECCTE, Organismes financeurs…)
*   Pour un échange d’informations, propositions sans être client de SOLSTICE SCOP : cinq ans

**6\. Sécurité et Protection des données**

Toutes les données collectées et stockées sur tous types de supports sont gérées par **SOLSTICE SCOP** en tant que responsable des traitements. Elles sont protégées et sécurisées par des mesures techniques et organisationnelles appropriées et conformes au règlement et dispositions légales en vigueur et notamment :

*   La nomination de responsables de traitements ;
*   La sensibilisation/formation aux exigences du règlement (RGPD) ;
*   La sécurisation des locaux et du système d’informations ;
*   La présente charte de confidentialité ;
*   Des exigences en matière de choix des sous-traitants, fournisseurs et partenaires ;
*   L’encadrement des transferts et données conservées dans l’UE.

Vos données personnelles sont protégées contre les risques d’altération, l’utilisation et/ou communication non autorisée, la perte accidentelle ou piratage.

**7\. Vos droits concernant vos données personnelles collectées :**

*   Droit d’accès et d’interrogation (Art.15 du RGPD) : vous avez le droit de demander l’accès à vos informations personnelles afin, par exemple, de connaître le type de traitement dont elles font l’objet.
*   Droit de rectification (Art.16 du RGPD) : vous pouvez demander la correction de données inexactes ou incomplètes.
*   Droit d’effacement (Art.17 du RGPD) : vous pouvez demander la suppression de vos données personnelles si elles ne sont plus indispensables au traitement ou à la finalité prévue lors de la collecte et si aucune base légale ne justifie la conservation.
*   Droit à la limitation du traitement (Art.18 du RGPD) : vous pouvez demander de limiter le traitement de vos données dans certains cas. Par exemple, si ces données sont erronées ou illégales, ou si vous souhaitez uniquement recevoir les offres et non une lettre d’information…
*   Droit à la portabilité (Art.20 du RGPD) : vous pouvez demander et vous faire mettre à disposition les données personnelles que vous avez fournies et ce dans un format et support lisible (électronique).
*   Droit d’opposition (Art.21 du RGPD) : vous pouvez vous opposer à tout moment à ce qu’un organisme utilise certaines de vos données.

**8\. Comment exercer ces droits ?**

La loi Informatique et libertés permet à toute personne d’accéder aux données qui la concernent. Ce droit est renforcé avec le Règlement général sur la protection des données (RGPD) qui est entré en application en mai 2018.

Vous pouvez exercer les droits mentionnés ci-dessus en justifiant de votre identité, sur une simple demande écrite par courrier adressée à : SOLSTICE SCOP, Direction générale, Écosite, ronde des Alisiers, 26400 EURRE, ou par courriel : [accueil@solstice.coop](mailto:accueil@solstice.coop).

Pour répondre à votre requête, nous pouvons vous demander de nous fournir des informations complémentaires pour vérifier votre identité. Nous donnerons suite à votre demande, dans un délai raisonnable, et en respectant les délais fixés par la loi.

En cas de litige, vous pouvez adresser une réclamation auprès de la Commission nationale informatique et libertés (CNIL).

Pour une information plus détaillée sur l’exercice de vos droits, vous pouvez consulter le site de la CNIL : [https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles](https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles)

[Visualiser et télécharger le document en format PDF](/files/Solstice_Protection-des-donnees_2019.pdf){target="_blank"}
