---
title: Catalogue des formations d'entrepreneur·es Solstice
eleventyNavigation:
  key: catalogue
  title: Catalogue
  parent: Organisme de formation
scripts:
  - /js/controllers/tags.js
---

:::info Catalogue en cours de mise à jour
Le catalogue des formations est en train d'être alimenté par les formateur·ices. \
N'hésitez pas à nous contacter si vous cherchez une formation spécifique.
:::

{% from "partials/_tags_filter.njk" import tags_filter %}
{{ tags_filter(trainings | extractTags) }}

{% from "partials/_tags_list.njk" import tags_list %}
{%- set max_tags = 10 -%}

<div id="entries">
{%- for training in trainings | sort(false, false, 'name') -%}
  {% set training_url = "/formations/catalogue/" + training.ref + "-" + training.name | slugify + "/" %}
  <article class="masonry-item card shadow-hover mb-3" data-id="{{ training.ref }}" data-order="{{ loop.index0 }}" data-tags="{{ training.tag_names }}">
    <div class="card-body">
      <h2 class="card-title h4"><a href="{{ training_url }}" class="underline-hover">{{ training.name }}</a></h2>
      <ul class="list-unstyled mb-0">
        <li class="mb-2">
          <i class="fas fa-user-circle text-secondary me-1" aria-hidden="true"></i>
          par
          {%- for user in training.users -%}
            {%- if loop.index0 > 0 %} et {% endif %}
            {%- if user.ref %}
              <a href="/entrepreneures/{{ user.name | slugify }}/" class="underline-hover fw-bold">{{ user.name | striptags }}</a>
            {% else %}
              {{ user.name | striptags }}
            {% endif %}
          {%- endfor -%}
        </li>
        {%- if training.duration -%}
          <li class="mb-2">
            <i class="fas fa-clock text-secondary me-1" aria-hidden="true"></i>
            Durée : {{ training.duration }}
          </li>
        {%- endif -%}
        {%- if training.tag_names | length -%}
          <li class="mb-2 clearfix">
            <div class="float-left">
              <i class="fas fa-tags text-secondary me-1" aria-hidden="true"></i>
              Étiquettes :
            </div>
            <div class="ms-4 ps-2">
              {{ tags_list(training.tag_names, max_tags) }}
            </div>
          </li>
        {%- endif -%}
        {%- if training.notes | length -%}
          <li class="mb-2">
            <div class="float-left pe-2">
              <i class="fas fa-info-circle text-secondary me-1" aria-hidden="true"></i>
            </div>
            <div class="ms-4">
              {{ training.notes | safe }}
            </div>
          </li>
        {%- endif -%}
        {%- if training.price -%}
          <li class="mb-2">
            <i class="fas fa-tag text-secondary me-1" aria-hidden="true"></i>
            Prix catalogue : {{ training.price }} (hors frais de déplacement, d'hébergement, etc.) {{ '(prix libre possible)' if training.open_price }}
          </li>
        {%- endif -%}
        <li>
          <i class="fas fa-arrow-circle-right text-secondary me-1" aria-hidden="true"></i>
          <a href="{{ training_url }}" title="{{ training.name }}" class="underline-hover">En savoir plus</a>
        </li>
      </ul>
    </div>{{- '' -}}
  </article>
{%- endfor -%}
</div>
