---
title: Conception et crédits
---

## Développement et méthodologie projet

Le site a été conçu avec une **approche pas à pas** (dite [agile]), et **centrée sur des besoins utilisateurs** — par exemple, "rejoindre la coopérative", "comprendre le statut de salarié-entrepreneur·e", "rendre service aux coopérateur·ices", etc.

La réalisation s'est faite en tandem entre personne salariée de la structure et salarié·e-entrepreneur·e.

- **Temps total de travail** : {{ temps.total | inHours }} heures ({{ temps.total | inDays }} jours ouvrés)
- Temps passé par **Goulven** : {{ temps.goulven | inHours }} heures ({{ temps.goulven | inDays }} jours ouvrés)
- Temps passé par **Sofia** : {{ temps.sofia | inHours }} heures ({{ temps.sofia | inDays }} jours ouvrés)
- Temps passé par **Thomas** : {{ temps.thomas | inHours }} heures ({{ temps.thomas | inDays }} jours ouvrés)
- Temps passé par **Valérie** : {{ temps.valérie | inHours }} heures ({{ temps.valérie | inDays }} jours ouvrés)

Ce [décompte est consigné dans un tableur](https://gitlab.com/solstice.coop/www/-/blob/main/le-temps-qui-passe.csv) depuis le 21 octobre 2021.

Le [code source de ce site est en libre accès](https://gitlab.com/solstice/www).\
Le [travail à faire est suivi dans ce tableau](https://gitlab.com/solstice.coop/www/-/boards).

## Logiciels libres

Merci aux personnes qui ont conçu ces briques logicielles sur leur temps libre,
ou dans le cadre de leur activité professionnelle.  

- [Eleventy](https://www.11ty.dev)
- [csv-parse](https://www.npmjs.com/package/csv-parse)
- [ical.js](https://github.com/mozilla-comm/ical.js)
- [Nextcloud](https://nextcloud.com/)
- [FontAwesome](https://fontawesome.com/)
- [Bootstrap](https://getbootsrap.com/)

## Services

- [GitLab](https://gitlab.com/gitlab-org/gitlab)
- [Scopen](https://www.scopen.fr/)

[agile]: https://manifesteagile.fr/
