---
title: Contacter une personne chez Solstice
eleventyNavigation:
  key: Contact
  parent: Accueil
  order: 70
---

{%- from "form.html" import form with context -%}

# Contactez-nous

- [prendre rendez-vous avec un·e accompagnant·e ?](#accompagnant)
- [en savoir plus sur la formation de formateur·trice ?](#formation-formateurice)
- [en savoir plus tout court ?](#autre)
- [L'équipe support](#equipe)

## Prendre rendez-vous avec un·e accompagnant·e ? { #accompagnant }

{% include 'contact/rendez-vous-accompagnant.njk' %}

## En savoir plus sur la formation de formateur·ice ? { #formation-formateurice }

{{ form(id=2, title=false, submit="J'envoie ma demande") }}

## En savoir plus tout court ! { #autre }

{{ form(id=1, title=false, submit="J'envoie mon message") }}

## L'équipe support { #equipe }

{% from "partials/_tags_list.njk" import tags_list %}

{% for person in support %}
{%- set name = [person.first_name, person.last_name] | join(' ') | capitalize -%}
<div class="card mb-3">
  <div class="card-body">
    <div class="d-flex flex-column flex-md-row">
      <div class="flex-grow-1 order-1 order-md-0">
        <h3 class="card-title mb-0 text-capitalize">
          {{ name }}
        </h3>
        {%- if person.title -%}
        <p>
          <strong>Activités/Métiers</strong> : {{ person.title }}
        </p>
        {%- endif -%}
        {% from "partials/_tags_list.njk" import tags_list %}
        {%- call tags_list(person.tag_names) -%}
        <p class="float-start mb-0">
          <strong>Étiquettes</strong> :
        </p>
        {%- endcall -%}
        <br class="clearfix">
        <div class="content mt-n3">{{ person.presentation | safe }}</div>
      </div>
      {%- set src = person.photo_link or person.logo_link -%}
      {%- if src -%}
      <div class="flex-shrink-0 w-md-25 order-0 order-md-1 mb-2 mb-md-0 ms-md-2">
        {% image src, name, { class: "img-fluid d-block mx-auto" } %}
      </div>
      {%- endif -%}
    </div>
  </div>
</div>
{% endfor %}
